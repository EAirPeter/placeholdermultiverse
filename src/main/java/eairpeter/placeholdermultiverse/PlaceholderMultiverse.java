package eairpeter.placeholdermultiverse;

import me.clip.deluxechat.placeholders.DeluxePlaceholderHook;
import me.clip.deluxechat.placeholders.PlaceholderHandler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MVWorldManager;

public class PlaceholderMultiverse extends JavaPlugin {

	private static final String ID = "pmultiverse";
	private static final String MVC = "Multiverse-Core";
	private static final String DCHAT = "DeluxeChat";
	
	private MVWorldManager wman = null;
	
	@Override
	public void onEnable() {
		PluginManager pman = Bukkit.getPluginManager();
		if (pman.isPluginEnabled(MVC))
			wman = ((MultiverseCore) pman.getPlugin(MVC)).getMVWorldManager();
		else
			disable(MVC + " not found!");
		if (pman.isPluginEnabled(DCHAT)) {
			boolean hooked = PlaceholderHandler.registerPlaceholderHook(ID, new DeluxePlaceholderHook() {

				@Override
				public String onPlaceholderRequest(Player p, String identifier) {
					switch (identifier) {
					case "worldalias":
						return wman.getMVWorld(p.getWorld()).getAlias();
					case "worldname":
						return wman.getMVWorld(p.getWorld()).getName();
					}
					return null;
				}
				
			});
			if (hooked)
				getLogger().info("DeluxeChat placeholder hook was successfully registered");
			else
				disable("Failed to registaer DeluxeChat placeholder");
		}
		else
			disable(DCHAT + " not found!");
	}
	
	@Override
	public void onDisable() {
		PlaceholderHandler.unregisterPlaceholderHook(ID);
	}
	
	private void disable(String reason) {
		getLogger().severe(reason);
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
}
