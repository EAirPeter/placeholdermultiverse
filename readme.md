PlaceholderMultiverse
=====================
This bukkit plugin adds placeholders which supports Multiverse-Core to DeluxeChat.

Placeholders
------------
* ```pmulticraft_worldalias```
	The alias of the world where the player is.
* ```pmulticraft_worldname```
	The name of the world where the player is.
